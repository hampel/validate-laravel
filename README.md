Additional Laravel Validation Rules
===================================

[![Latest Version on Packagist](https://img.shields.io/packagist/v/hampel/validate-laravel.svg?style=flat-square)](https://packagist.org/packages/hampel/validate-laravel)
[![Total Downloads](https://img.shields.io/packagist/dt/hampel/validate-laravel.svg?style=flat-square)](https://packagist.org/packages/hampel/validate-laravel)
[![Open Issues](https://img.shields.io/bitbucket/issues/hampel/validate-laravel.svg?style=flat-square)](https://bitbucket.org/hampel/validate-laravel/issues)
[![License](https://img.shields.io/packagist/l/hampel/validate-laravel.svg?style=flat-square)](https://packagist.org/packages/hampel/validate-laravel)

Custom Validators for Laravel

By [Simon Hampel](https://twitter.com/SimonHampel).

Installation
------------

The recommended way of installing this validation library is through [Composer](http://getcomposer.org):

Require the package via Composer in your `composer.json`

    {
        "require": {
            "hampel/validate-laravel": "^2.8"
        }
    }

Run Composer to update the new requirement.

    $ composer update

The package is built to work with the Laravel Framework.

Notes
-----

Version 2.8 of this library renames the __bool__ rule to __bool_equiv__ to avoid conflicts with the core bool rule.

Version 2.0 of this library removes several rules previously available:

**unique_or_zero** and **exists_or_zero** have been removed as they were deemed to be of little value

__domain__, **domain_in**, __tld__ and **tld_in** have been moved to a new package
[hampel/tlds](https://packagist.org/packages/hampel/tlds).

The getTlds helper function has also been removed and you should install the hampel/tlds package to gain access to all
new functions for retrieving an up-to-date list of all valid TLDs directly from IANA or other sources.

Usage
-----

This package adds additional validators for Laravel - refer to
[Laravel Documentation - Validation](http://laravel.com/docs/validation) for general usage instructions.

__bool_equiv__

The field under validation must be the equivalent of a "boolean" (either `true` or `false`) in a variety of forms.
Acceptable values include: "1", "true", "on" and "yes", "0", "false", "off", "no", "", and NULL

**ipv4_public**

The field under validation must be a public IPv4 address - ie. not in the "private" or "reserved" ranges.

**ipv6_public**

The field under validation must be a public IPv6 address - ie. not in the "private" or "reserved" ranges.

**ip_public**

The field under validation must be a public IPv4 or IPv6 address - ie. not in the "private" or "reserved" ranges.

**uploaded_file**

The field under validation must be an uploaded file of type `Symfony\Component\HttpFoundation\File\UploadedFile`, as
returned from `Input::file()`. The file upload must also be valid, that is, the upload must have succeeded with an error
return of `UPLOAD_ERR_OK`
(see [File Upload Error Messages Explained](http://php.net/manual/en/features.file-upload.errors.php) for more details
on file upload errors)
