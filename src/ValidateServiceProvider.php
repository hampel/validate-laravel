<?php namespace Hampel\Validate\Laravel;

use Hampel\Validate\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Hampel\Validate\Laravel\Validation\ValidatorExtensions;

class ValidateServiceProvider extends ServiceProvider
{
	protected $rules = [
		'bool_equiv', 'ipv4_public', 'ipv6_public', 'ip_public', 'uploaded_file'
	];

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register(){}

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->defineTranslations();

		$this->app->singleton(ValidatorExtensions::class, function()
		{
			return new ValidatorExtensions(new Validator());
		});

		$this->addNewRules();
	}

	protected function defineTranslations()
	{
		$this->loadTranslationsFrom(__DIR__ . '/../lang', 'validate-laravel');

		$this->publishes([
            __DIR__ . '/../lang' => $this->app->langPath('vendor/validate-laravel')
		], 'lang');
	}

	protected function addNewRules()
	{
		foreach ($this->rules as $rule)
		{
			$this->extendValidator($rule);
		}
	}

	protected function extendValidator($rule)
	{
		$method = 'validate' . Str::studly($rule);
		$translation = $this->app['translator']->get('validate-laravel::validation');

		$this->app['validator']->extend($rule, ValidatorExtensions::class . "@{$method}", $translation[$rule]);
	}
}
