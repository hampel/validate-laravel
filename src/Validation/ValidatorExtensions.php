<?php namespace Hampel\Validate\Laravel\Validation;

use Hampel\Validate\Validator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ValidatorExtensions
{
	/**
	 * @var \Hampel\Validate\Validator
	 */
	protected $validator;

	public function __construct(Validator $validator)
	{
		$this->validator = $validator;
	}

	public function validateBoolEquiv($attribute, $value, $parameters)
	{
		return $this->validator->isBool($value);
	}

	public function validateIpv4Public($attribute, $value, $parameters)
	{
		return $this->validator->isPublicIpv4($value);
	}

	public function validateIpv6Public($attribute, $value, $parameters)
	{
		return $this->validator->isPublicIpv6($value);
	}

	public function validateIpPublic($attribute, $value, $parameters)
	{
		return $this->validator->isPublicIp($value);
	}

	protected function validateUploadedFile($attribute, $value, $parameters)
	{
		return ($value instanceof UploadedFile AND $value->isValid());
	}
}
