<?php namespace Hampel\Validate\Laravel\Validation;

use Hampel\Validate\Validator;
use PHPUnit\Framework\TestCase;
use Illuminate\Validation\Factory;
use Illuminate\Contracts\Support\MessageBag;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Translation\Translator;

use Mockery as m;

class ValidatorExtensionsTest extends TestCase
{
	use \Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;

	public function testValidateBool()
	{
		$validator = m::mock(Validator::class);

		$extensions = new ValidatorExtensions($validator);

		$container = m::mock(Container::class);
		$translator = m::mock(Translator::class);

		$container->shouldReceive('make')->once()->with(ValidatorExtensions::class)->andReturn($extensions);
		$validator->shouldReceive('isBool')->once()->with('yes')->andReturn(true);

		$factory = new Factory($translator, $container);
		$factory->extend('bool_equiv', ValidatorExtensions::class . '@validateBoolEquiv', ':attribute must be a boolean equivalent');
		$validator = $factory->make(['foo' => 'yes'], ['foo' => 'bool_equiv']);
		$this->assertTrue($validator->passes());
	}

	public function testValidateBoolFails()
	{
		$validator = m::mock(Validator::class);

		$extensions = new ValidatorExtensions($validator);

		$container = m::mock(Container::class);
		$translator = m::mock(Translator::class);

		$container->shouldReceive('make')->once()->with(ValidatorExtensions::class)->andReturn($extensions);
		$validator->shouldReceive('isBool')->once()->with('bar')->andReturn(false);
		$translator->shouldReceive('get')->once()->with('validation.custom.foo.bool_equiv')->andReturn('validation.custom.foo.bool_equiv');
		$translator->shouldReceive('get')->once()->with('validation.custom')->andReturn('validation.custom');
		$translator->shouldReceive('get')->once()->with('validation.bool_equiv')->andReturn('validation.bool_equiv');
		$translator->shouldReceive('get')->once()->with('validation.attributes')->andReturn('validation.attributes');
		$translator->shouldReceive('get')->once()->with('validation.values.foo.bar')->andReturn('validation.values.foo.bar');

		$factory = new Factory($translator, $container);
		$factory->extend('bool_equiv', ValidatorExtensions::class . '@validateBoolEquiv', ':attribute must be a boolean equivalent');
		$validator = $factory->make(['foo' => 'bar'], ['foo' => 'bool_equiv']);
		$this->assertTrue($validator->fails());

		$messages = $validator->messages();
		$this->assertInstanceOf(MessageBag::class, $messages);
		$this->assertEquals('foo must be a boolean equivalent', $messages->first('foo'));
	}
}
